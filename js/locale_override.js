/**
 * @file
 * JavaScript behaviors for locale override module.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  var initialized = false;
  /**
   * Append locale overrides to drupal translation strings.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.localeOverrideTranslations = {
    attach: function () {
      if (initialized) {
        return;
      }

      window.drupalTranslations = window.drupalTranslations || {};
      window.drupalTranslations.strings = window.drupalTranslations.strings || {};
      var strings = window.drupalTranslations.strings;
      for (var context in drupalSettings.locale_override.strings) {
        strings[context] = strings[context] || {};
        $.extend(strings[context], drupalSettings.locale_override.strings[context]);
      }
      initialized = true;
    }
  };

})(jQuery, Drupal, drupalSettings);
