/**
 * @file
 * JavaScript behaviors for locale override (admin) elements.
 */

(function ($, Drupal, once) {

  'use strict';

  /**
   * Attach handlers to locale override strings element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.localeOverrideStringsAadmin = {
    attach: function (context) {
      const $elements =  $(once('locale-override-string-sync', '.js-locale-override-string-sync', context));
      $elements.each(function () {
        // Target input name and not id because the id will be changing via
        // Ajax callbacks.
        var name = this.name;

        var $source = $(this);
        var $translation = $('textarea[name="' + name.replace(/\[source\]$/, '[translation]') + '"]');

        // On focus, determine if source and translation text are in-sync.
        $source.on('focus', function () {
          var sync = $source.val() === $translation.val();
          $source.data('locale_override_string_sync', sync);
          if (sync) {
            $translation.prop('readonly', true).closest('.js-form-item, .js-form-wrapper').addClass('webform-readonly');
          }
        });

        // On blur, if source and translation are in-sync remove readonly.
        $source.on('blur', function () {
          if ($source.data('locale_override_string_sync')) {
            $translation.prop('readonly', false).closest('.js-form-item, .js-form-wrapper').removeClass('webform-readonly');
          }
        });

        // On keyup, if source and translation  text are in-sync then set
        // source to translations.
        $source.on('keyup', function () {
          if ($source.data('locale_override_string_sync')) {
            $translation.val($source.val());
          }
        });

      });
    }
  };

})(jQuery, Drupal, once);
