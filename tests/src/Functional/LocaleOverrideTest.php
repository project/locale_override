<?php

namespace Drupal\Tests\locale_override\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for locale override.
 *
 * @group locale_override
 */
class LocaleOverrideTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['locale_override', 'locale_override_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests locale override.
   */
  public function testLocaleOverride() {
    $assert_session = $this->assertSession();

    $user = $this->createUser(['administer locale override', 'translate configuration']);

    /* ********************************************************************** */

    // Check that access is denied to users with
    // 'administer locale override' permission.
    $this->drupalGet('/admin/config/regional/translate_override');
    $assert_session->statusCodeEquals(403);

    // Login user with 'administer locale override' permission.
    $this->drupalLogin($user);

    // Check that access is allowed to users with
    // 'administer locale override' permission.
    $this->drupalGet('/admin/config/regional/translate_override');
    $assert_session->responseNotContains('"locale_override":');
    $assert_session->responseNotContains('Context [English]');
    $assert_session->responseNotContains('JavaScript [English]');

    $assert_session->statusCodeEquals(200);

    // Import 'Context' and 'JavaScript' strings which are the column headers.
    $edit = ['strings' => implode(PHP_EOL, ['Context', 'JavaScript'])];
    $this->drupalGet('/admin/config/regional/translate_override/import');
    $this->submitForm($edit, 'Import strings');
    $assert_session->responseContains('The strings have been imported.');

    // Update the 'Context' and 'JavaScript' strings and append [English].
    $edit = [
      'strings[items][0][translation]' => 'Context [English]',
      'strings[items][1][translation]' => 'JavaScript [English]',
      'strings[items][1][javascript]' => TRUE,
    ];
    $this->drupalGet('/admin/config/regional/translate_override');
    $this->submitForm($edit, 'Save translations');
    $assert_session->responseContains('The strings have been saved.');

    // Check that the locale override English strings are being
    // used as expected.
    $assert_session->responseContains('"locale_override":{"strings":{"":{"JavaScript":"JavaScript [English]"}}}');
    $assert_session->responseContains('Context [English]');
    $assert_session->responseContains('JavaScript [English]');

    // Append [Spanish] to  'Context' and 'JavaScript' strings.
    $edit = [
      'translation[config_names][locale_override.settings][strings][ad4e206408c1c59bfb8a057ba7857446][translation]' => 'Context [Spanish]',
      'translation[config_names][locale_override.settings][strings][686155af75a60a0f6e9d80c1f7edd3e9][translation]' => 'JavaScript [Spanish]',
    ];
    $this->drupalGet('/admin/config/regional/translate_override/translate/es/add');
    $this->submitForm($edit, 'Save translation');
    $assert_session->responseContains('Successfully saved Spanish translation.');

    // Check that the locale override Spanish strings are being
    // used as expected.
    $this->drupalGet('/es/admin/config/regional/translate_override/translate/es/edit');
    $assert_session->responseContains('"locale_override":{"strings":{"":{"JavaScript":"JavaScript [Spanish]"}}}');
    $assert_session->responseContains('Context [Spanish]');
  }

}
