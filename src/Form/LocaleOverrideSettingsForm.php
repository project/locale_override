<?php

namespace Drupal\locale_override\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Locale Override configuration settings form.
 */
class LocaleOverrideSettingsForm extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->languageManager = $container->get('language_manager');
    $instance->cacheTagsInvalidator = $container->get('cache_tags.invalidator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'locale_override_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['locale_override.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $language = $this->languageManager->getCurrentLanguage();
    $is_english_default = ($language->isDefault() && $language->getId() === 'en');
    $config = $this->config('locale_override.settings');

    $form['strings'] = [
      '#type' => 'webform_multiple',
      '#title' => $this->t('Translated strings'),
      '#title_display' => 'hidden',
      '#required' => TRUE,
      '#header' => TRUE,
      '#min_items' => 1,
      '#add_more_items' => 5,
      '#add_more_input_label' => $this->t('more strings'),
      '#element' => [
        'source' => [
          '#type' => 'textarea',
          '#required' => TRUE,
          '#title' => $this->t('Source (English) string'),
          '#placeholder' => $this->t('Enter source string…'),
          '#attributes' => ($is_english_default) ? ['class' => ['js-locale-override-string-sync']] : [],
          '#rows' => 2,
        ],
        'translation' => [
          '#type' => 'textarea',
          '#required' => TRUE,
          '#title' => ($is_english_default)
            ? $this->t('Override for @language', ['@language' => $language->getName()])
            : $this->t('Translation for @language', ['@language' => $language->getName()]),
          '#placeholder' => ($is_english_default)
            ? $this->t('Enter override string…')
            : $this->t('Enter translation string…'),
          '#rows' => 2,
        ],
        'context' => [
          '#type' => 'textfield',
          '#title' => $this->t('Context'),
        ],
        'javascript' => [
          '#type' => 'checkbox',
          '#title' => $this->t('JavaScript'),
        ],
      ],
      '#default_value' => $config->get('strings'),
    ];

    $form['#attached']['library'][] = 'locale_override/locale_override.admin';

    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#value'] = $this->t('Save translations');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $strings = $form_state->getValue('strings');
    foreach ($strings as $index => $string) {
      $string['javascript'] = (boolean) $string['javascript'];
      // Convert strings numeric indexes to md5 hash of the source + context
      // strings.
      //
      // Dots are explicitly forbidden in configuration data keys now.
      // @see https://www.drupal.org/node/2297311
      $strings[md5($string['source'] . $string['context'])] = $string;
      unset($strings[$index]);
    }

    // Save config.
    $this->config('locale_override.settings')
      ->set('strings', $strings)
      ->save();

    // Clear cache.
    $this->cacheTagsInvalidator->invalidateTags(['local_override']);

    // Display message.
    $this->messenger()->addStatus($this->t('The strings have been saved.'));
  }

}
