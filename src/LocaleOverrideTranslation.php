<?php

namespace Drupal\locale_override;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

/**
 * Provides locale (string) overrides.
 */
class LocaleOverrideTranslation implements TranslatorInterface  {

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manger.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a LocaleOverrideTranslation.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   */
  public function __construct(CacheBackendInterface $cache, CacheTagsInvalidatorInterface $cache_tags_invalidator, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager) {
    $this->cache = $cache;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    $translations = $this->getLanguage($langcode);
    if (isset($translations[$context][$string])) {
      return $translations[$context][$string];
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->cacheTagsInvalidator->invalidateTags(['local_override']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getLanguage($langcode) {
    $cid = 'locale_overrides:strings.' . $langcode;
    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $translations = [];
    $strings = $this->configFactory->get('locale_override.settings')->get('strings') ?: [];
    foreach ($strings as $string) {
      if ($string['source'] !== $string['translation']) {
        $translations[$string['context']][$string['source']] = $string['translation'];
      }
    }

    // Only cache the translated string when current language matches the translated language.
    if ($this->languageManager->getCurrentLanguage()->getId() === $langcode) {
      $this->cache->set($cid, $translations, Cache::PERMANENT, ['local_override']);
    }

    return $translations;
  }

}
