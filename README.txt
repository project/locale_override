Locale Override
---------------

The Locale Override module provides a very simple user interface to store
translated  user interface strings as config entities. Storing translated
strings in config entities makes it easier and possible for a Translated
Management Systems (TMS) to translate user interface strings.


REQUIREMENTS
------------

* This module requires Webform modules outside of the Drupal core.
  (https://www.drupal.org/project/locale_override)

* Set up user permissions. (/admin/people/permissions#module-locale_override)


INSTALLATION
------------

* Install the module as you would normally install a contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* After enable, Locale Override makes use of that feature by adding stores
  translated user interface strings in a config entity.


MAINTAINERS
-----------

* Jacob Rockowitz (jrockowitz) - https://www.drupal.org/u/jrockowitz
